#time complexity: O(n^2)
# n is number of items

#algorithm description:
# 1. sort items by value/weight
# 2. for each item, try to add it to the knapsack
# 3. if the knapsack is full, stop

#
def BranchAndBound(data, Result):
    Pos = []
    Ratio = []

    for i in range(data.Size()):
        Pos.append(i)
        Ratio.append(data.m_Value[i] / data.m_Weight[i])

    # Sort all of them together base on Ratio
    Zipped = zip(Pos, Ratio, data.m_Value, data.m_Weight, data.m_Label)
    SortedData = sorted(Zipped, key = lambda x : x[1], reverse = True)
    Pos, Ratio, data.m_Value, data.m_Weight, data.m_Label = zip(*SortedData)

    Ratio = list(Ratio)
    Ratio.append(0)

    for x in range(data.Size()):
        ValueWithoutX = data.CalcValue(Result) + (data.Capacity() - data.CalcWeight(Result)) * Ratio[x + 1]

        Result[x] = 1
        if (data.CalcWeight(Result) <= data.Capacity()):
            ValueWithX = data.CalcValue(Result) + (data.Capacity() - data.CalcWeight(Result)) * Ratio[x + 1]

            if (ValueWithoutX > ValueWithX):
                Result[x] = 0
        else:
            Result[x] = 0

    # Sort all of them base on original position
    Zipped = zip(Pos, Result, data.m_Value, data.m_Weight, data.m_Label)
    SortedData = sorted(Zipped, key = lambda x : x[0])
    Pos, NewResult, data.m_Value, data.m_Weight, data.m_Label = zip(*SortedData)

    Result.clear()
    for i in range(data.Size()):
        Result.append(NewResult[i])

    if (not data.InAllClass(Result)):
        Result.clear()
        for i in range(data.Size()):
            Result.append(0)
   


def BranchAndBound_Main(data, FileParameter):
    Result = [0] * data.Size()

    BranchAndBound(data, Result)

    data.WriteResult(FileParameter, Result)